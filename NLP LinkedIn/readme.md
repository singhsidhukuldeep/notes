# NLP with Python for Machine Learning Essential Training

https://www.linkedin.com/learning/nlp-with-python-for-machine-learning-essential-training

## Topics that come under NLP

* Sentiment Analysis

* Topic Modeling

* Text classification

* Sentence segmentation or POS (Part os speech) tagging

* more...

## SPAM filter

1. Raw text -model can't distinguish words

2. Tokenize -tell the model what to look at

3. Clean text - remove stop words/ punctuation, stemming, etc

4. Vectorize - convert to numeric form

5. Machine learning algorithm - fit/train model

6. Spam filter - system to filter emails

## nltk (natural language toolkit)

### Installing

assuming python is installed

```
pip install -U nltk
```

```
import nltk
nltk.download()
```

```
dir(nltk) #lists everything
```

### popular:

* pos_tag

* tokenize (splitting into a list of words)

### stopwords

```
from nltk.corpus import stopwords
stopwords.words('english')[0:500:25]
```

## Our working data

```
# Read in the raw text
rawData = open("SMSSpamCollection.tsv").read()

# Print the raw data
rawData[0:500]
```

```
"ham\tI've been searching for the right words to thank you for this breather. I promise i wont take your help for granted and will fulfil my promise. You have been wonderful and a blessing at all times.\nspam\tFree entry in 2 a wkly comp to win FA Cup final tkts 21st May 2005. Text FA to 87121 to receive entry question(std txt rate)T&C's apply 08452810075over18's\nham\tNah I don't think he goes to usf, he lives around here though\nham\tEven my brother is not like to speak with me. They treat me like aid"
```


## reading data

```
# Read in the raw text
rawData = open("SMSSpamCollection.tsv").read()

# Print the raw data
rawData[0:500]

# Splitiing data
parsedData = rawData.replace('\t', '\n').split('\n')

# taking every other value to form the columns
labelList = parsedData[0::2]
textList = parsedData[1::2]

# creating a pandas dataframe
# make sure that all the lists that you add in the dataframe are of the same size
import pandas as pd
fullCorpus = pd.DataFrame({
    'label': labelList[:-1],
    'body_list': textList
})
fullCorpus.head()

# Directly creating a dataframe from the file
dataset = pd.read_csv("SMSSpamCollection.tsv", sep="\t", header=None)
fullCorpus.columns = ['label', 'body_text']
dataset.head()

# What is the shape of the dataset?
print("Input data has {} rows and {} columns".format(len(fullCorpus), len(fullCorpus.columns)))

# How many spam/ham are there?
print("Out of {} rows, {} are spam, {} are ham".format(len(fullCorpus), len(fullCorpus[fullCorpus['label']=='spam']), len(fullCorpus[fullCorpus['label']=='ham'])))

# How much missing data is there?
print("Number of null in label: {}".format(fullCorpus['label'].isnull().sum()))
print("Number of null in text: {}".format(fullCorpus['body_text'].isnull().sum()))
```

## Regular expressions

### Usefule methods for tokenising

* findall()

* split()

### Useful regexes for tokenising

* `\W` & `\w` - words (W not words and w words)

* `\S` & `\s` - whitespaces (s spaces and S not spaces)

```
import re

re_test = 'This is a made up string to test 2 different regex methods'
re_test_messy = 'This      is a made up     string to test 2    different regex methods'
re_test_messy1 = 'This-is-a-made/up.string*to>>>>test----2""""""different~regex-methods'
```


```
re.split('\s', re_test)
re.split('\s', re_test_messy)
re.split('\s+', re_test_messy)# add plus for more than 1 spaces
re.split('\W+', re_test_messy1) # add plus for more than 1 not words
```

```
re.findall('\S+', re_test)
re.findall('\w+', re_test_messy1)
```

### Other examples of regex methods

- re.search()
- re.match()
- re.fullmatch()
- re.finditer()
- re.escape()

### Replacing a specific string

```
pep8_test = 'I try to follow PEP8 guidelines'
pep7_test = 'I try to follow PEP7 guidelines'
peep8_test = 'I try to follow PEEP8 guidelines'
```

```
re.findall('[a-z]+', pep8_test) # all smallcaps cases
re.findall('[A-Z]+', pep8_test) # all caps cases
re.findall('[A-Z0-9]+', peep8_test) # all cap alpabest OR numbers
re.findall('[A-Z]+[0-9]+', peep8_test) # all cap alpabest AND numbers
re.sub('[A-Z]+[0-9]+', 'PEP8 Python Styleguide', peep8_test) # substitute
```

## Implementing a pipeline to clean text

### Pre-processing text data

Cleaning up the text data is necessary to highlight attributes that you're going to want your machine learning system to pick up on. Cleaning (or pre-processing) the data typically consists of a number of steps:
1. **Remove punctuation**
2. **Tokenization**
3. **Remove stopwords**
4. **Lemmatize/Stem**

![final](images/final_cleaned.png)

The first three steps are covered in this chapter as they're implemented in pretty much any text cleaning pipeline. Lemmatizing and stemming are covered in the next chapter as they're helpful but not critical.

```
import pandas as pd
pd.set_option('display.max_colwidth', 150) # setting column width
data = pd.read_csv("SMSSpamCollection.tsv", sep='\t', header=None)
data.columns = ['label', 'body_text']
data.head()
```

### Remove puctuation

```
import string
string.punctuation
# '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
```

```
def remove_punct(text):
    text_nopunct = "".join([char for char in text if char not in string.punctuation])
    # using join to create a string back from list of characters, we join on nothing
    return text_nopunct

data['body_text_clean'] = data['body_text'].apply(lambda x: remove_punct(x))

data.head()
```

![no punctuation](images/no_punctuation.png)

### Tokenization

```
import re

def tokenize(text):
    tokens = re.split('\W+', text)
    return tokens

data['body_text_tokenized'] = data['body_text_clean'].apply(lambda x: tokenize(x.lower()))

data.head()
```

![Tokenized](images/tokenized.png)

### Remove stopwords

```
import nltk
stopword = nltk.corpus.stopwords.words('english')
```

```
def remove_stopwords(tokenized_list):
    text = [word for word in tokenized_list if word not in stopword]
    return text

data['body_text_nostop'] = data['body_text_tokenized'].apply(lambda x: remove_stopwords(x))

data.head()
```

![no stopwords](images/no_stop.png)

### Stemming

Stemming is the process of reducing inflected or derived words to their word stem or root.

More simply put, the process of stemming means often crudely chopping off the end of a word, to leave only the base.

Eg:Stemming and stemmed is stem and electricity and electrical is elec

fails sometime: meanness/meaning >>mean

#### Stemmers provided by nltk

* **Porter stemmer** //most popular//

* Snowball stemmer

* Lancaster stemmer

* Regex-based stemmer

```
import nltk
ps = nltk.PorterStemmer()
dir(ps)
```

```
print(ps.stem('grows'))
print(ps.stem('growing'))
print(ps.stem('grow'))

grow
grow
grow

print(ps.stem('run'))
print(ps.stem('running'))
print(ps.stem('runner'))

run
run
runner
```

```
import pandas as pd
import re
import string
pd.set_option('display.max_colwidth', 100)

stopwords = nltk.corpus.stopwords.words('english')

data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

data.head()

# Cleaning
def clean_text(text):
    text = "".join([word for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [word for word in tokens if word not in stopwords]
    return text

data['body_text_nostop'] = data['body_text'].apply(lambda x: clean_text(x.lower()))

data.head()
```

```
def stemming(tokenized_text):
    text = [ps.stem(word) for word in tokenized_text]
    return text

data['body_text_stemmed'] = data['body_text_nostop'].apply(lambda x: stemming(x))

data.head()
```

![stemmed](images/stemmed.png)

### Lemmatizing

Process of grouping together the inflected forms of word, identified by word's lemma

Using vocabulary analysis of words aiming to remove inflectional endings to return the dictionary form of the word.

**How is lemmatizing different from stemming**

The goal of both is to condense derived words in to there base forms

* Stemming is typically faster as it simply chops of the end of a word using heuristics, without any understanding of the context in which the word was used

* Lemmatizing is typically more accurate as it uses more informed analysis to create group of words with similar meaning based on the context around the word.  this is a bit slow

```
import nltk
wn = nltk.WordNetLemmatizer() #using wordnet
ps = nltk.PorterStemmer()
dir(wn)
```

```
print(ps.stem('meanness'))
print(ps.stem('meaning'))
mean
mean

print(wn.lemmatize('meanness'))
print(wn.lemmatize('meaning'))
meanness
meaning

print(ps.stem('goose'))
print(ps.stem('geese'))
goos
gees

print(wn.lemmatize('goose'))
print(wn.lemmatize('geese'))
goose
goose
```

```
import pandas as pd
import re
import string
pd.set_option('display.max_colwidth', 100)

stopwords = nltk.corpus.stopwords.words('english')

data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

data.head()

def clean_text(text):
    text = "".join([word for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [word for word in tokens if word not in stopwords]
    return text

data['body_text_nostop'] = data['body_text'].apply(lambda x: clean_text(x.lower()))

data.head()
```

```
def lemmatizing(tokenized_text):
    text = [wn.lemmatize(word) for word in tokenized_text]
    return text

data['body_text_lemmatized'] = data['body_text_nostop'].apply(lambda x: lemmatizing(x))

data.head(10)
```

![lemmatized](images/lemmatized.png)

## Vectorizing raw data

### Different types

* Count vectorization

* N*grams

* Term frequency - inverse document frequency (TF-IDF)

### Count vectorization

```
# reading
import pandas as pd
import re
import string
import nltk
pd.set_option('display.max_colwidth', 100)
stopwords = nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()
data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

#cleaning
def clean_text(text):
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [ps.stem(word) for word in tokens if word not in stopwords]
    return text
```

```
from sklearn.feature_extraction.text import CountVectorizer

count_vect = CountVectorizer(analyzer=clean_text)
X_counts = count_vect.fit_transform(data['body_text'])
print(X_counts.shape)
print(count_vect.get_feature_names())
```

_Difference between `fit and fit_transform`
If we only fit it, it won't actually do anything to our data. It'll just train the vectorizer object to learn what words are in the corpus. So if we want to actually fit it, and then transform our data, in other words, if we want to fit the vectorizer, and then actually vectorize our data and turn them into feature vectors, then we'll need to call fit_transform, and that'll actually do the fitting and transform our data. So what will be stored in X_counts, is the vectorized version of the data._

#### Apply CountVectorizer to smaller sample

```
data_sample = data[0:20]

count_vect_sample = CountVectorizer(analyzer=clean_text)
X_counts_sample = count_vect_sample.fit_transform(data_sample['body_text'])
print(X_counts_sample.shape)
print(count_vect_sample.get_feature_names())
```

#### Vectorizers output sparse matrices

_**Sparse Matrix**: A matrix in which most entries are 0. In the interest of efficient storage, a sparse matrix will be stored by only storing the locations of the non-zero elements._

```
X_counts_sample

X_counts_df = pd.DataFrame(X_counts_sample.toarray())
X_counts_df
X_counts_df.columns = count_vect_sample.get_feature_names()
X_counts_df
```

![Count Vector final](images/count_vect_final.png)

### N Grams

### N-Grams 

Creates a document-term matrix where counts still occupy the cell but instead of the columns representing single terms, they represent all combinations of adjacent words of length n in your text.

"NLP is an interesting topic"

| n | Name      | Tokens                                                         |
|---|-----------|----------------------------------------------------------------|
| 2 | bigram    | ["nlp is", "is an", "an interesting", "interesting topic"]      |
| 3 | trigram   | ["nlp is an", "is an interesting", "an interesting topic"] |
| 4 | four-gram | ["nlp is an interesting", "is an interesting topic"]    |

```
# reading
import pandas as pd
import re
import string
import nltk
pd.set_option('display.max_colwidth', 100)
stopwords = nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()
data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

#cleaning
def clean_text(text):
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = " ".join([ps.stem(word) for word in tokens if word not in stopwords])
    return text
data['cleaned_text'] = data['body_text'].apply(lambda x: clean_text(x))
data.head()
```

#### Apply CountVectorizer (w/ N-Grams)

```
#Apply CountVectorizer (w/ N-Grams)

from sklearn.feature_extraction.text import CountVectorizer
ngram_vect = CountVectorizer(ngram_range=(2,2))
X_counts = ngram_vect.fit_transform(data['cleaned_text'])
print(X_counts.shape)
print(ngram_vect.get_feature_names())
```

_`ngram_vect = CountVectorizer(ngram_range=(2,2))` bigrams_

_`ngram_vect = CountVectorizer(ngram_range=(1,2))` unigrams and bigrams_

_`ngram_vect = CountVectorizer(ngram_range=(1,3))` unigrams, bigrams and tri grams_

#### Apply CountVectorizer (w/ N-Grams) to smaller sample

```
data_sample = data[0:20]
ngram_vect_sample = CountVectorizer(ngram_range=(2,2))
X_counts_sample = ngram_vect_sample.fit_transform(data_sample['cleaned_text'])
print(X_counts_sample.shape)
print(ngram_vect_sample.get_feature_names())
```

```
X_counts_df = pd.DataFrame(X_counts_sample.toarray())
X_counts_df.columns = ngram_vect_sample.get_feature_names()
X_counts_df
```

![n-gram final](images/ngram_final.png)

### Inverse document frequency weighting

Creates a document-term matrix where the columns represent single unique terms (unigrams) but the cell represents a weighting meant to represent how important a word is to a document.

**_Term Frequency-Inverse Document Frequency (TF-IDF)_**

TF-IDF creates a document term matrix, where there's still one row per text message and the columns still represent single unique terms. But instead of the cells representing the count, the cells represent a weighting that's meant to identify how important a word is to an individual text message.

![TF-IDF](images/TF-IDF.png)

![TF-IDF](images/TF-IDFexample.png)

```
# reading data
import pandas as pd
import re
import string
import nltk
pd.set_option('display.max_colwidth', 100)
stopwords = nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()
data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

# cleaning data
def clean_text(text):
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [ps.stem(word) for word in tokens if word not in stopwords]
    return text
```

#### Apply TfidfVectorizer

```
from sklearn.feature_extraction.text import TfidfVectorizer

tfidf_vect = TfidfVectorizer(analyzer=clean_text)
X_tfidf = tfidf_vect.fit_transform(data['body_text'])
print(X_tfidf.shape)
print(tfidf_vect.get_feature_names())
```

#### Apply TfidfVectorizer to a smaller sample

```
data_sample = data[0:20]

tfidf_vect_sample = TfidfVectorizer(analyzer=clean_text)
X_tfidf_sample = tfidf_vect_sample.fit_transform(data_sample['body_text'])
print(X_tfidf_sample.shape)
print(tfidf_vect_sample.get_feature_names())
```

```
X_tfidf_df = pd.DataFrame(X_tfidf_sample.toarray())
X_tfidf_df.columns = tfidf_vect_sample.get_feature_names()
X_tfidf_df
```

![TF-IDF final](images/TF-IDFfinal.png)

## Feature Engineering

Creating new features or transforming your existing features to get the most out of your data.

### Example Transformations

* Power transformations (square, square root, etc)

* Standardizing data

### Transformation process

1. Determine what range of exponents to test

2. Plly each transformation to each value of your chosen feature

3. Use some criteria to determine which of the transformations yield the best distribution (closer to normal so our model can use it)

#### Reading data

```
import pandas as pd
data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']
```

#### Create feature for text message length

```
data['body_len'] = data['body_text'].apply(lambda x: len(x) - x.count(" "))
data.head()
```

#### Create feature for % of text that is punctuation

```
import string
def count_punct(text):
    count = sum([1 for char in text if char in string.punctuation])
    return round(count/(len(text) - text.count(" ")), 3)*100
data['punct%'] = data['body_text'].apply(lambda x: count_punct(x))
data.head()
```

#### Evaluate created features

```
from matplotlib import pyplot
import numpy as np
%matplotlib inline

bins = np.linspace(0, 200, 40)
pyplot.hist(data[data['label']=='spam']['body_len'], bins, alpha=0.5, normed=True, label='spam')
pyplot.hist(data[data['label']=='ham']['body_len'], bins, alpha=0.5, normed=True, label='ham')
pyplot.legend(loc='upper left')
pyplot.show()

bins = np.linspace(0, 50, 40)
pyplot.hist(data[data['label']=='spam']['punct%'], bins, alpha=0.5, normed=True, label='spam')
pyplot.hist(data[data['label']=='ham']['punct%'], bins, alpha=0.5, normed=True, label='ham')
pyplot.legend(loc='upper right')
pyplot.show()
```

![Features Created](images/features_created.png)

![Length Distribution](images/LengthDistribution.png)

![Per Distribution](images/PerDistribution.png)

### Box-Cox Power Transformation

**Base Form**: $$ y^x $$

| X    | Base Form           |           Transformation               |
|------|--------------------------|--------------------------|
| -2   | $$ y ^ {-2} $$           | $$ \frac{1}{y^2} $$      |
| -1   | $$ y ^ {-1} $$           | $$ \frac{1}{y} $$        |
| -0.5 | $$ y ^ {\frac{-1}{2}} $$ | $$ \frac{1}{\sqrt{y}} $$ |
| 0    | $$ y^{0} $$              | $$ log(y) $$             |
| 0.5  | $$ y ^ {\frac{1}{2}}  $$ | $$ \sqrt{y} $$           |
| 1    | $$ y^{1} $$              | $$ y $$                  |
| 2    | $$ y^{2} $$              | $$ y^2 $$                |


**Process**
1. Determine what range of exponents to test
2. Apply each transformation to each value of your chosen feature
3. Use some criteria to determine which of the transformations yield the best distribution

## Machine Learning

### Hold out test set

Sample of data not used in fitting a model for the purpose of evaluating the model's ability to generalize unseen data.

### K-Fold Cross-Validation

The full data se is divided into k-subsets and the holdout method is repeated k times. Each time, one of the k-subsets is used as the test set and the other k-1 subsets are put together to be used to train the model.

### Evaluation Metrics

![Evaluation Metrics](images/eval.png)


![Precision and Recall](images/precisionRecall.png)

### Random Forest

_**Ensemble Method**_

_Technique that creates multiple models and then combines them to produce better results than any of the single methods indvidually_

Random forest is an ensemble learning method that constructs a collection of decision trees and then aggregates the predictions of each tree to determine the final prediction.

So in this case, your weak models are the individual decision trees, and then those are combined into the strong model that is the aggregated random forest model.

**Benifits of Ensemble Methods**

* Can be used for classification and regression

* Easily handles outliers, missing values, etc

* Accepts various types of inputs (continous, ordibal, etc.)

* Less likely to overfit

* Outputs feature importance

#### Reading and cleaning data

```
import nltk
import pandas as pd
import re
from sklearn.feature_extraction.text import TfidfVectorizer
import string

stopwords = nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()

data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

def count_punct(text):
    count = sum([1 for char in text if char in string.punctuation])
    return round(count/(len(text) - text.count(" ")), 3)*100

data['body_len'] = data['body_text'].apply(lambda x: len(x) - x.count(" "))
data['punct%'] = data['body_text'].apply(lambda x: count_punct(x))

def clean_text(text):
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [ps.stem(word) for word in tokens if word not in stopwords]
    return text

tfidf_vect = TfidfVectorizer(analyzer=clean_text)
X_tfidf = tfidf_vect.fit_transform(data['body_text'])

X_features = pd.concat([data['body_len'], data['punct%'], pd.DataFrame(X_tfidf.toarray())], axis=1)
X_features.head()
```

#### Explore RandomForestClassifier Attributes & Hyperparameters

```
from sklearn.ensemble import RandomForestClassifier

print(dir(RandomForestClassifier))
print(RandomForestClassifier())
```

#### Explore RandomForestClassifier through Cross-Validation

```
from sklearn.model_selection import KFold, cross_val_score

rf = RandomForestClassifier(n_jobs=-1)
k_fold = KFold(n_splits=5)
cross_val_score(rf, X_features, data['label'], cv=k_fold, scoring='accuracy', n_jobs=-1)
```

_random forest is built on relatively few fully built decision trees._

_n_jobs equal to negative one for any process that can be run in parallel_

_scikit-learn always expects you to input your X_features and your label separately_

#### Explore RandomForestClassifier through Holdout Set

```
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_features, data['label'], test_size=0.2)

from sklearn.ensemble import RandomForestClassifier

rf = RandomForestClassifier(n_estimators=50, max_depth=20, n_jobs=-1)
rf_model = rf.fit(X_train, y_train)

# Importance of each feature
sorted(zip(rf_model.feature_importances_, X_train.columns), reverse=True)[0:10]

y_pred = rf_model.predict(X_test)
precision, recall, fscore, support = score(y_test, y_pred, pos_label='spam', average='binary')

print('Precision: {} / Recall: {} / Accuracy: {}'.format(round(precision, 3),
                                                        round(recall, 3),
                                                        round((y_pred==y_test).sum() / len(y_pred),3)))
```

_train test split function will output four datasets, so we have to tell it what to name those datasets, or what we want it to output that data to. So the first one that it'll output is X train, and then the next one that it'll output is X test, and then the next one that it'll output is Y train, and then lastly, it will output Y test. And it's always in that order_

_score is outputting four different values, we need to give it four different places to store each of those values. And it's outputted in the exact same order that the raw function name is in. So it outputs precision, and then recall, and then F score, and then support_

#### Altering hyper parameters to get the best result

**Grid-search:** Exhaustively search all parameter combinations in a given grid to determine the best model.

```
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_features, data['label'], test_size=0.2)

def train_RF(n_est, depth):
    rf = RandomForestClassifier(n_estimators=n_est, max_depth=depth, n_jobs=-1)
    rf_model = rf.fit(X_train, y_train)
    y_pred = rf_model.predict(X_test)
    precision, recall, fscore, support = score(y_test, y_pred, pos_label='spam', average='binary')
    print('Est: {} / Depth: {} ---- Precision: {} / Recall: {} / Accuracy: {}'.format(
        n_est, depth, round(precision, 3), round(recall, 3),
        round((y_pred==y_test).sum() / len(y_pred), 3)))

for n_est in [10, 50, 100]:
    for depth in [10, 20, 30, None]:
        train_RF(n_est, depth)
```

```
# Our output
Est: 10 / Depth: 10 ---- Precision: 1.0 / Recall: 0.216 / Accuracy: 0.892
Est: 10 / Depth: 20 ---- Precision: 0.975 / Recall: 0.516 / Accuracy: 0.932
Est: 10 / Depth: 30 ---- Precision: 1.0 / Recall: 0.647 / Accuracy: 0.952
Est: 10 / Depth: None ---- Precision: 0.984 / Recall: 0.784 / Accuracy: 0.969
Est: 50 / Depth: 10 ---- Precision: 1.0 / Recall: 0.235 / Accuracy: 0.895
Est: 50 / Depth: 20 ---- Precision: 1.0 / Recall: 0.562 / Accuracy: 0.94
Est: 50 / Depth: 30 ---- Precision: 1.0 / Recall: 0.667 / Accuracy: 0.954
Est: 50 / Depth: None ---- Precision: 0.985 / Recall: 0.843 / Accuracy: 0.977
Est: 100 / Depth: 10 ---- Precision: 1.0 / Recall: 0.242 / Accuracy: 0.896
Est: 100 / Depth: 20 ---- Precision: 1.0 / Recall: 0.601 / Accuracy: 0.945
Est: 100 / Depth: 30 ---- Precision: 0.981 / Recall: 0.686 / Accuracy: 0.955
Est: 100 / Depth: None ---- Precision: 1.0 / Recall: 0.83 / Accuracy: 0.977
```

**Cross-validation (cv):** Divide a dataset into k subsets and repeat the holdout method k times where a different subset is used as the holdout set in each iteration.

#### Reading text

```
import nltk
import pandas as pd
import re
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import string

stopwords = nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()

data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

def count_punct(text):
    count = sum([1 for char in text if char in string.punctuation])
    return round(count/(len(text) - text.count(" ")), 3)*100

data['body_len'] = data['body_text'].apply(lambda x: len(x) - x.count(" "))
data['punct%'] = data['body_text'].apply(lambda x: count_punct(x))

def clean_text(text):
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [ps.stem(word) for word in tokens if word not in stopwords]
    return text

# TF-IDF
tfidf_vect = TfidfVectorizer(analyzer=clean_text)
X_tfidf = tfidf_vect.fit_transform(data['body_text'])
X_tfidf_feat = pd.concat([data['body_len'], data['punct%'], pd.DataFrame(X_tfidf.toarray())], axis=1)

# CountVectorizer
count_vect = CountVectorizer(analyzer=clean_text)
X_count = count_vect.fit_transform(data['body_text'])
X_count_feat = pd.concat([data['body_len'], data['punct%'], pd.DataFrame(X_count.toarray())], axis=1)

X_count_feat.head()
```

#### GridSearchCV (Grid search cross validation)

```
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

rf = RandomForestClassifier()
param = {'n_estimators': [10, 150, 300],
        'max_depth': [30, 60, 90, None]}

gs = GridSearchCV(rf, param, cv=5, n_jobs=-1)
gs_fit = gs.fit(X_tfidf_feat, data['label'])
pd.DataFrame(gs_fit.cv_results_).sort_values('mean_test_score', ascending=False)[0:5]

rf = RandomForestClassifier()
param = {'n_estimators': [10, 150, 300],
        'max_depth': [30, 60, 90, None]}

gs = GridSearchCV(rf, param, cv=5, n_jobs=-1)
gs_fit = gs.fit(X_count_feat, data['label'])
pd.DataFrame(gs_fit.cv_results_).sort_values('mean_test_score', ascending=False)[0:5]
```

### Gradient Boosting

Ensemble learning method that takes an iterative approach to combining weak learners to create a strong learner by focusing on mistakes of prior iterations. 

![Evaluation Metrics](images/randomvGradient.png)

![Evaluation Metrics](images/GBprosCons.png)

_Focusing on the one that it is failing_

##### Explore GradientBoostingClassifier Attributes & Hyperparameters

```
from sklearn.ensemble import GradientBoostingClassifier
print(dir(GradientBoostingClassifier))
print(GradientBoostingClassifier())

from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_features, data['label'], test_size=0.2)

def train_GB(est, max_depth, lr):
    gb = GradientBoostingClassifier(n_estimators=est, max_depth=max_depth, learning_rate=lr)
    gb_model = gb.fit(X_train, y_train)
    y_pred = gb_model.predict(X_test)
    precision, recall, fscore, train_support = score(y_test, y_pred, pos_label='spam', average='binary')
    print('Est: {} / Depth: {} / LR: {} ---- Precision: {} / Recall: {} / Accuracy: {}'.format(
        est, max_depth, lr, round(precision, 3), round(recall, 3), 
        round((y_pred==y_test).sum()/len(y_pred), 3)))

for n_est in [50, 100, 150]:
    for max_depth in [3, 7, 11, 15]:
        for lr in [0.01, 0.1, 1]:
            train_GB(n_est, max_depth, lr)
```

**Evaluate Gradient Boosting with GridSearchCV**

```
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV

gb = GradientBoostingClassifier()
param = {
    'n_estimators': [100, 150], 
    'max_depth': [7, 11, 15],
    'learning_rate': [0.1]
}

clf = GridSearchCV(gb, param, cv=5, n_jobs=-1)
cv_fit = clf.fit(X_tfidf_feat, data['label'])
pd.DataFrame(cv_fit.cv_results_).sort_values('mean_test_score', ascending=False)[0:5]
```

## Final Model Selection

Vectorizers vectorizes the word in the training set andwhen it works on test set it only works on words that were there in the training set

**Reading and Cleaning**

```
import nltk
import pandas as pd
import re
from sklearn.feature_extraction.text import TfidfVectorizer
import string

stopwords = nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()

data = pd.read_csv("SMSSpamCollection.tsv", sep='\t')
data.columns = ['label', 'body_text']

def count_punct(text):
    count = sum([1 for char in text if char in string.punctuation])
    return round(count/(len(text) - text.count(" ")), 3)*100

data['body_len'] = data['body_text'].apply(lambda x: len(x) - x.count(" "))
data['punct%'] = data['body_text'].apply(lambda x: count_punct(x))

def clean_text(text):
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [ps.stem(word) for word in tokens if word not in stopwords]
    return text
```


**Now we will split before vectorizing**

```
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(data[['body_text', 'body_len', 'punct%']], data['label'], test_size=0.2)
```

**Vectorize text**

```
tfidf_vect = TfidfVectorizer(analyzer=clean_text)
tfidf_vect_fit = tfidf_vect.fit(X_train['body_text'])

tfidf_train = tfidf_vect_fit.transform(X_train['body_text'])
tfidf_test = tfidf_vect_fit.transform(X_test['body_text'])

X_train_vect = pd.concat([X_train[['body_len', 'punct%']].reset_index(drop=True), 
           pd.DataFrame(tfidf_train.toarray())], axis=1)
X_test_vect = pd.concat([X_test[['body_len', 'punct%']].reset_index(drop=True), 
           pd.DataFrame(tfidf_test.toarray())], axis=1)

X_train_vect.head()
```

**Final evaluation of models**

```
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import precision_recall_fscore_support as score
import time

rf = RandomForestClassifier(n_estimators=150, max_depth=None, n_jobs=-1)

rf_model = rf.fit(X_train_vect, y_train)
y_pred = rf_model.predict(X_test_vect)

precision, recall, fscore, train_support = score(y_test, y_pred, pos_label='spam', average='binary')
print('Precision: {} / Recall: {} / Accuracy: {}'.format(
    round(precision, 3), round(recall, 3), round((y_pred==y_test).sum()/len(y_pred), 3)))

gb = GradientBoostingClassifier(n_estimators=150, max_depth=11)

gb_model = gb.fit(X_train_vect, y_train)
y_pred = gb_model.predict(X_test_vect)

precision, recall, fscore, train_support = score(y_test, y_pred, pos_label='spam', average='binary')
print('Precision: {} / Recall: {} / Accuracy: {}'.format(
    round(precision, 3), round(recall, 3), round((y_pred==y_test).sum()/len(y_pred), 3)))
```

![Final Points](images/finalPoints.png)