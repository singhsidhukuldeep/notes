# How to Win a Data Science Competition: Learn from Top Kagglers

https://www.coursera.org/learn/competitive-data-science?specialization=aml

## Popular ML alogorithms

* **Linear** Good for sparse high dimensional data - vowpal wabbit(good for large  data sets), scikit learn
 * Logistic refression
 * SVM (Support Vector Machines)
* **Tree based** Good and powerful for tabular data (but hard to capture Linear dependencies since it requires a lots of splits). - scikit learn for random forest, XGBoost and light GBM for GBDT for higher speed and accuracy
 * Decision Tree
 * Random Forest
 * GBDT (Gradient Boosted Decision Trees)
* **kNN**
* **Neural Networks**
