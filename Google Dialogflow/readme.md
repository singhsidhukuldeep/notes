# Google Dialogflow

Video: https://www.youtube.com/watch?v=DkZmVLHoCLo

https://dialogflow.com/

## Main Components of DialogFlow

* Intents

* Contexts

* Entities

### **Intents**

How an agent determines what to do with the input!

 - What Parameters to save!

 - Make call to fulfillment or not

 - Establish context to guide conversation

**_Types of intents_**

* General Intent

* Fallback Intent (When you don't know something)

Each intent has context assosciated with it, an *Input context* and an *Output Context*

So an intent can add context OR remove context

**Two approaches**

* Model your intents after what your **user** hopes to acheive (*Eg. playSong*)

* Mode your intents after what your **agent** jopes to receive (*Eg. getName*)

**Machine Learning**

_In Dialogf;pw terminology, your agent uses machine learning algorithms to match user requests to specific intents and uses entities to extract relevant data from them_

### **Contexts**

*They drive your conversation*, What you have at any given time or what intents can be matched.

Context can be used to control the flow of conversation

**Each intent has contexts**

* _Input Context_ Intent will only be matched if within this context

* _Output Context_ Contains information extracted from current intent, for example the matched values will be accessible (*Eg. haveName.givenName will return the givenName value from the haveName context*)

![Intent Context](images/contextIntent.png)

### **Entities**

Entities are the types of values that get matched and recognised in the interface

**_Types of Entities_**

* System Defined

* Developer Defined

**Allow automated extension** to add missing variables Eg. Icecream flavours
*Eg I want a scoop of Durian will add Durian as flavour if not already there but if someone says I want a scoop of Kevin it will also add Kevin which is wrong*

**Define synonyms** *Eg. Coco, chocolate, etc.*