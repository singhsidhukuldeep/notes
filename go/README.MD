# GO Lang (Coursera)

## Advantages of GO Lang

* **Code runs fast:** since it is complied to machine code whenever the code is complied (this only happens once)
* **Garbage collection**
* **Simpler Objects**
* **Concurrency is efficient**

*Java is partially complied language as when you the compile the code it generates byte code rather than machine code. Then the byte code is interpretted at run time by JVM (Java Virtual Machine)*

## Object Orientation

* Go doesnot use the term `class` but uses `structs`
* NO inheritance, constructors, generics

## Recommended Code Organisation

You can have more than one project in a single Workspace

**workspace directory is defined by the `GOPATH` variable**

go tools assume that your code is in `GOPATH` for `windows` it's generally `C:\Users\yourname\go`

first line of the file names the package

there must be one package called main which has function `main()` where the code execution starts

### Workspace
- **src** contains source code files
- **pkg** contains packages/libraries
- **bin** contains executables (complied code)

## Go tool

* `go build` compiles the program. (you can also pass a list of .go files that need to be compiled), creates an executable for the main package with the same name as the first .go file.
* `go doc` prints the documentaion for a package
* `go fmt` formats the source code files (indentation)
* `go get` get's and installs new packages
* `go list` lits all the installed packages
* `go run` compiles and executes
* `go test` runs test using files ending in `*_test.go`

## Variables

must have name and a type

```
var x int
var x, y int
```

### type declarations

defining an alias/ alternate name for a type

```
type Celsius float64
type IDnum int

var temp Celsius
```

### initializing variables

```
var x int = 100
var x = 100

var y int
y = 1

var x int // x = 0
var x string // x = ""
```

### short variable declarations

you can only do inside a function

```
X:=100
```
