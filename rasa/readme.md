# Rasa (NLU + Core)

Rasa has two main modules:

* NLU for understanding user messages in natural language and extracting useful information from the text
* Core for holding conversations and deciding what to do next


YouTube Link: https://youtu.be/xu6D_vLP5vY

Rasa-Core is the one that connects with the API.

## Install requirements

`pip install -r requirements.txt`

## Spacy

**Spacy** Download: `python -m spacy download en`

## Rasa NLU trainer

GUI for training - **Requires NodeJS and NPM (Node Package Manager)**

`npm i -g rasa-nlu-trainer`

To open GUI: **Go to the *data folder* and open the GUI**

`rasa-nlu-trainer`

This opens the data.json file

## Training RASA NLU Model

This will make the BOT understand intent and pick up entities from the data.

You need:

* **Data** to train the model. Generally data is stored in `../data/data.json`
* **Config** file for instructions generally stored in: `../data/config_spacy.json` but can also be a `.yml` file
* **Script** that trains the model. Generally stored in `nlu_model.py`

### Data

*Example* `../data/data.json`
```
{
  "rasa_nlu_data": {
    "common_examples": [
      {
        "text": "Hello",
        "intent": "greet",
        "entities": []
      },
      {
        "text": "goodbye",
        "intent": "goodbye",
        "entities": []
      },
      {
        "text": "What's the weather in Berlin at the moment?",
        "intent": "inform",
        "entities": [
          {
            "start": 22,
            "end": 28,
            "value": "Berlin",
            "entity": "location"
          }
        ]
      },
      {
        "text": "what is the weather?",
        "intent": "inform",
        "entities": []
      },
      {
        "text": "Tell me the weather",
        "intent": "inform",
        "entities": []
      },
}
```

To open GUI: **Go to the *data folder* and open the GUI**

`rasa-nlu-trainer`

This opens the data.json file

### Config file

*Example* `../config_spacy.json`
```
{
  "pipeline":"spacy_sklearn",
  "path":"./models/nlu",
  "data":"./data/data.json"
}
```

**pipeline** tells what feature extractors will be used to crunch the messages provided in the data and extract necessary information.
*There are two main pipelines **sklearn** and **Mitie** .*

**path** tells where the model will be saved, this is a *folder*

**data** tells where the data is located

### Script

#### Training Model
*Example* `../nlu_model.py`
```
from rasa_nlu.converters import load_data
from rasa_nlu.config import RasaNLUConfig
from rasa_nlu.model import Trainer

def train_nlu(data, config, model_dir):
	training_data = load_data(data)
	trainer = Trainer(RasaNLUConfig(config))
	trainer.train(training_data)
	model_directory = trainer.persist(model_dir, fixed_model_name = 'weathernlu')

if __name__ == '__main__':
	train_nlu('./data/data.json', 'config_spacy.json', './models/nlu')
```

#### Running Model (Testing) `../nlu_model.py`

*Example* `../nlu_model.py`
```
from rasa_nlu.converters import load_data
from rasa_nlu.config import RasaNLUConfig
from rasa_nlu.model import Trainer
from rasa_nlu.model import Metadata, Interpreter ## Need this Only for RUNNING

def train_nlu(data, config, model_dir):
	training_data = load_data(data)
	trainer = Trainer(RasaNLUConfig(config))
	trainer.train(training_data)
	model_directory = trainer.persist(model_dir, fixed_model_name = 'weathernlu')

def run_nlu():
	interpreter = Interpreter.load('./models/nlu/default/weathernlu', RasaNLUConfig('config_spacy.json'))
	print(interpreter.parse("I am planning my holiday to Lithuania. I wonder what is the weather out there."))

if __name__ == '__main__':
	#train_nlu('./data/data.json', 'config_spacy.json', './models/nlu') ## Commented out training
	run_nlu()

```

## Training RASA Core Dialogue Management Model

This will make the BOT reply naturally to the queries

You need:

* **domain** is a universe where the chatbot lives and operates. `../weather_domain.yml`
* **actions.py** for custom actions `../actions.py`
* **stories.md** actual/natural conversations b/w user and bot `../stories.md` *It is always a markdown file*
* **script** that trains the model

### domain

Domain contains 5 things:

* **slots** the things that needed to be kept track of (basically context)
* **intents** all the intentions
* **entities** that the chatbot should be ready to get (if you add this to slot it will be kept through out the conversation)
* **templates** text responses that our chatbot should send back once specific actions from the user side are predicted
* **actions** the actions that our chatbot should be ready to perform (including all the templates) as well as the condition details (for eg returning weather). Since this will not be a very simple text response it is not added in templates

*Example* `../weather_domain.yml`

```
slots:
  location:
    type: text


intents:
 - greet
 - goodbye
 - inform


entities:
 - location

templates:
  utter_greet:
    - 'Hello! How can I help?'
  utter_goodbye:
    - 'Talk to you later.'
    - 'Bye bye :('
  utter_ask_location:
    - 'In what location?'


actions:
 - utter_greet
 - utter_goodbye
 - utter_ask_location
 - actions.ActionWeather

```

### actions

complex actions like api calls

don't forget to `dispatcher.utter_message` to return your message and also `SlotSet` to set your slots and also include thin in `domanin -- actions`

*Example* `../actions.py`
```
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_core.actions.action import Action
from rasa_core.events import SlotSet

class ActionWeather(Action):
	def name(self):
		return 'action_weather'

	def run(self, dispatcher, tracker, domain):
		from apixu.client import ApixuClient
		api_key = '...' #your apixu key
		client = ApixuClient(api_key)

		loc = tracker.get_slot('location')
		current = client.getcurrent(q=loc)

		country = current['location']['country']
		city = current['location']['name']
		condition = current['current']['condition']['text']
		temperature_c = current['current']['temp_c']
		humidity = current['current']['humidity']
		wind_mph = current['current']['wind_mph']

		response = """It is currently {} in {} at the moment. The temperature is {} degrees, the humidity is {}% and the wind speed is {} mph.""".format(condition, city, temperature_c, humidity, wind_mph)

		dispatcher.utter_message(response)
		return [SlotSet('location',loc)]

```

### stories

actual/natural conversations b/w user and bot

stateless stories are where there is just one user input and one BOT output

*Example* `../stories.md`
```
## Generated Story -5208991511085841103
    - slot{"location": "London"}
    - action_weather
* goodbye
    - utter_goodbye
    - export
## Generated Story -5208991511085841103
    - slot{"location": "London"}
    - action_weather
* goodbye
    - utter_goodbye
    - export
## story_001
* greet
   - utter_greet
* inform
   - utter_ask_location
* inform[location=London]
   - slot{"location": "London"}
   - action_weather
* goodbye
   - utter_goodbye
## story_002
* greet
```

### script

#### training

*Example* `../train_init.py`
```
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import logging

from rasa_core.agent import Agent ## training agent
from rasa_core.policies.keras_policy import KerasPolicy ## actual model (default keras is LSTM Long SHort Term Memory)
from rasa_core.policies.memoization import MemoizationPolicy ## actual model

if __name__ == '__main__':
	logging.basicConfig(level='INFO')

	training_data_file = './data/stories.md' ## where the data is
	model_path = './models/dialogue' ## where to save the model

	agent = Agent('weather_domain.yml', policies = [MemoizationPolicy(), KerasPolicy()])

	agent.train(
			training_data_file, ## training file
			augmentation_factor = 50, ## creating fake stories from the already given data/ can increase this is if already providing large amount of data
			max_history = 2, ## no. of states model should remember (small model less number of states)
			epochs = 500, ## no of passes
			batch_size = 10, ## amount of training examples used in each pass
			validation_split = 0.2 ## splitting for training and testing)

	agent.persist(model_path)

```

training while actually having conversations with the chatbot

*Example* `../train_online.py`
```
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from rasa_core.agent import Agent
from rasa_core.channels.console import ConsoleInputChannel ## to send mess to the bot
from rasa_core.interpreter import RegexInterpreter
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy
from rasa_core.interpreter import RasaNLUInterpreter ## that does all RASA NLU stuff

logger = logging.getLogger(__name__)


def run_weather_online(input_channel, interpreter,
                          domain_file="weather_domain.yml",
                          training_data_file='data/stories.md'):
    agent = Agent(domain_file,
                  policies=[MemoizationPolicy(), KerasPolicy()],
                  interpreter=interpreter)

    agent.train_online(training_data_file,
                       input_channel=input_channel,
                       max_history=2,
                       batch_size=50,
                       epochs=200,
                       max_training_samples=300)

    return agent


if __name__ == '__main__':
    logging.basicConfig(level="INFO")
    nlu_interpreter = RasaNLUInterpreter('./models/nlu/default/weathernlu')
    run_weather_online(ConsoleInputChannel(), nlu_interpreter)

```

training and running

*Example* `../dialogue_management_model.py`
```
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from rasa_core.agent import Agent
from rasa_core.channels.console import ConsoleInputChannel
from rasa_core.interpreter import RegexInterpreter
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy
from rasa_core.interpreter import RasaNLUInterpreter

logger = logging.getLogger(__name__)

def train_dialogue(domain_file = 'weather_domain.yml',
					model_path = './models/dialogue',
					training_data_file = './data/stories.md'):

	agent = Agent(domain_file, policies = [MemoizationPolicy(), KerasPolicy()])

	agent.train(
				training_data_file,
				max_history = 3,
				epochs = 300,
				batch_size = 50,
				validation_split = 0.2,
				augmentation_factor = 50)

	agent.persist(model_path)
	return agent

def run_weather_bot(serve_forever=True):
	interpreter = RasaNLUInterpreter('./models/nlu/default/weathernlu')
	agent = Agent.load('./models/dialogue', interpreter = interpreter)

	if serve_forever:
		agent.handle_channel(ConsoleInputChannel())

	return agent

if __name__ == '__main__':
	train_dialogue()
	run_weather_bot()

```

## NGROK

you can use ngrok to make Public URLs for exposing our local web server (local host).

https://ngrok.com/

if your app is running at `port:5050`

`ngrok http 5050`
