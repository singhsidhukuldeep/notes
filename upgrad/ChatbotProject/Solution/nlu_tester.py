# Rasa NLU testing for UpGrad restaurant chatbot project
import ast
import math



class TestNLU():

    def __init__(self, ground_truth, parsed_query):
        self.ground_truth = ground_truth # dict
        self.parsed_query = parsed_query # dict

    def check_intent(self):
        # actual intent
        actual_intent = self.ground_truth['intent'].lower()

        # parsed intent confidence and name
        parsed_intent_conf = self.parsed_query['intent']['confidence']
        parsed_intent_name = self.parsed_query['intent']['name'].lower()

        if actual_intent == parsed_intent_name and parsed_intent_conf >= 0.50:
            return (True, "actual intent is {0}, parsed intent is {1}".format(actual_intent, parsed_intent_name))
        return (False, "actual intent is {0}, parsed intent is {1}".format(actual_intent, parsed_intent_name))

    def check_entities(self,dictionary):

        # actual entities; list of dicts
        actual_entities = self.ground_truth['entities']

        # parsed entities; list of dicts
        parsed_entities = self.parsed_query['entities']
        for dictt in parsed_entities:
            for key in dictt.keys():
                if key == 'entity':
                    dictt[key] = dictionary.get(dictt[key])
        # if key == 'en':
        #     current_dict[key] = definition

        # compute lengths (number of intents)
        len_actual_entities = len(actual_entities)
        len_parsed_entities = len(parsed_entities)

        # compare actual and parsed entities
        entity_results = []
        for entity in actual_entities:
            entity_name = entity['entity'].lower()
            entity_value = entity['value'].lower()

            # extract all parsed entity names
            parsed_entity_names = [entity['entity'].lower() for entity in parsed_entities]
            # if entity name is found, extract the corresponding parsed entity value
            parsed_value = ''.join([entity.get('value').lower() for entity in parsed_entities if entity.get('entity')==entity_name])

            # check whether entity exists in parsed entities
            if entity_name not in parsed_entity_names:
                result = (False, "actual entity is {0}={1}, but it is not parsed".format(entity_name, entity_value), 
                    entity_name)
                entity_results.append(result)
            # check whether the value of the entity matches with actual
            elif entity_value != parsed_value:
                result = (False, "actual entity is {0}={1}, but parsed entity is {2}={3}".format(entity_name, entity_value,
                                                                                                 entity_name, parsed_value),
                entity_name)
                entity_results.append(result)
            else:
                result = (True, "actual entity is {0}={1}, and parsed entity is {2}={3}".format(entity_name, entity_value,
                                                                                                entity_name, parsed_value),
                entity_name)
                entity_results.append(result)
        if len(entity_results) == 0:
            return [(True, "No entities exist in this query.", "no entity")]
        return entity_results





