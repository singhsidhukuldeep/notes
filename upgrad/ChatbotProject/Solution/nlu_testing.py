import pandas as pd
import numpy as np
import os
import json, pprint, yaml
from rasa_nlu.training_data import load_data
from rasa_nlu.config import RasaNLUModelConfig
from rasa_nlu.model import Trainer
from rasa_nlu.model import Metadata, Interpreter
from nlu_tester import TestNLU
import numpy as np
import warnings
warnings.simplefilter("ignore", DeprecationWarning)


print("Check the domain file and input the names corresponding to each entity type")
print("For Location--")
location = input()
print("\n"+ "For Cuisine--")
cuisine = input()
print("\n"+"For Email--")
email = input()
print("\n"+"For price--")
price = input()

dictionary = {location: 'location',cuisine: 'cuisine',email: 'email', price: 'price'}

#print(dictionary)
# JSON: Training data for NLU
with open("./data/data.json", 'r') as f:
    nlu_training_data = json.load(f)

# pprint.pprint(nlu_training_data['rasa_nlu_data']['common_examples'])

# training examples; list of dicts
training_examples = nlu_training_data['rasa_nlu_data']['common_examples']

# load the trained model and initialize the interpreter
interpreter = Interpreter.load('./models/nlu/default/restaurantnlu')

nlu_results = []
# parse queries and store results
for sample in training_examples:
    # predict results using trained NLU model
    parsed_query = interpreter.parse(sample['text'])

    # NLU tester
    nlu_tester = TestNLU(ground_truth=sample, parsed_query=parsed_query)

    # store intent and entity results for each sample query
    result = {'query_text': sample['text'],
               'intent_score': nlu_tester.check_intent(),
               'entity_score': nlu_tester.check_entities(dictionary)}
    nlu_results.append(result)

# compute final intent and entity scores
intent_scores = [result['intent_score'][0] for result in nlu_results]
total_intent_score = sum(intent_scores)/len(intent_scores)
print("Total intent score: {0}".format(round(total_intent_score, 2)))

entity_scores = [entity_score[0] for result in nlu_results for entity_score in result['entity_score']]

# entity types wise scores
location_score = [entity_score[0] for result in nlu_results 
                                 for entity_score in result['entity_score'] 
                                 if entity_score[2]=="location"]

final_location_score = sum(location_score)/len(location_score)
print("final location_score: {0}".format(final_location_score) )
# cuisine
cuisine_score = [entity_score[0] for result in nlu_results 
                                 for entity_score in result['entity_score'] 
                                 if entity_score[2]=="cuisine"]
final_cuisine_score = sum(cuisine_score)/len(cuisine_score)
print("final cuisine score: {0}".format(final_cuisine_score) )

# budget
budget_score = [entity_score[0] for result in nlu_results 
                                 for entity_score in result['entity_score'] 
                                 if entity_score[2]=="price"]

budget_examples =["<300", "300-700", "price range >700", "less than 300"]
if sum(budget_score) ==0:
    budget_results = []
    results_budget =0
    for i in budget_examples:
        # predict results using trained NLU model
        parsed_query_budget = interpreter.parse(i)

        # email testing
        if len(parsed_query_budget['entities']) !=0:
            if parsed_query_budget['entities'][0]['entity'] == 'price':
                results_budget =parsed_query_budget['entities'][0]['confidence']

        budget_results.append(results_budget)
    final_budget_score = max(sum(budget_results)/len(budget_results),0)
else:
    final_budget_score = sum(budget_score)/len(budget_score)
print("final budget score: {0}".format(final_budget_score) )

# email
email_score = [entity_score[0] for result in nlu_results 
                                 for entity_score in result['entity_score'] 
                                 if entity_score[2]=="email"]

final_email_score = sum(email_score)/len(email_score)

print("final email score: {0}".format(final_email_score) )

total_entity_score = sum(entity_scores)/len(entity_scores)
print("Total entity score: {0}".format(round(total_entity_score, 2)))

# write results in a text file
with open("nlu_results.txt", 'w') as f:
    for result in nlu_results:
        f.write('''query text: {0}\nintent result: {1}\nentity result: {2}\n\n'''.format(result['query_text'],
                                                      result['intent_score'],
                                                      result['entity_score']))



