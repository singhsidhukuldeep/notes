# Java Learning LinkedIn Learning

https://www.linkedin.com/learning/learning-java-3

## Java Libraries

* `Java.lang` Fundamental to the core Java language (math, boolean, byte)

* `Java.util` Generic utilities (scanning, formatting strings, data manipulation)

* `Java.net` Infrastructure for networking

```
// JAVA Code
import java.awt.*;
import java.lang.reflect.Array;
import java.util.concurrent.Future;
import java.util.Scanner;
import java.util.Arrays;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        double power = Math.pow(5, 3);
        System.out.println(power);

        double squareRoot = Math.sqrt(64);
        System.out.println(squareRoot);

        Random rand = new Random();
        int randomNumber = rand.nextInt();
        int randomNumberWithBound = rand.nextInt(10);
        System.out.println(randomNumber);
        System.out.println(randomNumberWithBound);
    }

}
```

## Encapsulation

`Chapter04 > 04_02 > end`

* Encapsulation is the process of wrapping data and methods into a single unit (class)

* A way to make programs more secure

* Prevent unauthorised members from accessing certain variables and methods