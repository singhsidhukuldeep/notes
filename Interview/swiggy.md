# Tail Recursion

The idea used by compilers to optimize tail-recursive functions is simple, since the recursive call is the last statement, there is nothing left to do in the current function, so saving the current function’s stack frame is of no use

## Tail recursive function to calculate **factorial**

```
# A tail recursive function 
# to calculate factorial 
def factTR(n, a): 
  
    if (n == 0): 
        return a 
  
    return factTR(n - 1, n * a) 
  
# A wrapper over factTR 
def fact(n): 
    return factTR(n, 1) 
  
# Driver program to test 
#  above function 
print(fact(5)) 
```

## Tail recursive function to calculate **fibnacci number**

```
# A tail recursive function to  
# calculate n th fibnacci number 
def fib(n, a = 0, b = 1): 
    if n == 0: 
        return a 
    if n == 1: 
        return b 
    return fib(n - 1, b, a + b); 
  
# Driver Code 
n = 9; 
print("fib("+str(n)+") = "+str(fib(n))) 
```

# Get the intersection point of two Linked Lists

## Mark Visited Nodes

Traverse the first linked list and keep marking visited nodes. Now traverse the second linked list, If you see a visited node again then there is an intersection point, return the intersecting node. This solution works in O(m+n) but requires additional information with each node

# Find frequency of each element in a limited range array in less than O(n) time

## Use Binary Search

 The idea is to recursively divide the array into two equal subarrays if its end elements are different. If both its end elements are same, that means that all elements in the subarray is also same as the array is already sorted.

```
// C++ program to count number of occurrences of 
// each element in the array in less than O(n) time 
#include <iostream> 
#include <vector> 
using namespace std; 
   
// A recursive function to count number of occurrences  
// for each element in the array without traversing  
// the whole array 
void findFrequencyUtil(int arr[], int low, int high,  
                        vector<int>& freq) 
{ 
    // If element at index low is equal to element  
    // at index high in the array 
    if (arr[low] == arr[high])  
    { 
        // increment the frequency of the element 
        // by count of elements between high and low 
        freq[arr[low]] += high - low + 1; 
    }  
    else
    { 
        // Find mid and recurse for left and right  
        // subarray 
        int mid = (low + high) / 2; 
        findFrequencyUtil(arr, low, mid, freq); 
        findFrequencyUtil(arr, mid + 1, high, freq); 
    } 
} 
   
// A wrapper over recursive function  
// findFrequencyUtil(). It print number of  
// occurrences of each element in the array. 
void findFrequency(int arr[], int n) 
{ 
    // create a empty vector to store frequencies 
    // and initialize it by 0. Size of vector is  
    // maximum value (which is last value in sorted 
    // array) plus 1. 
    vector<int> freq(arr[n - 1] + 1, 0); 
       
    // Fill the vector with frequency 
    findFrequencyUtil(arr, 0, n - 1, freq); 
   
    // Print the frequencies 
    for (int i = 0; i <= arr[n - 1]; i++) 
        if (freq[i] != 0) 
            cout << "Element " << i << " occurs "
                 << freq[i] << " times" << endl; 
} 
   
// Driver function 
int main() 
{ 
    int arr[] = { 1, 1, 1, 2, 3, 3, 5, 5, 
                  8, 8, 8, 9, 9, 10 }; 
    int n = sizeof(arr) / sizeof(arr[0]); 
   
    findFrequency(arr, n); 
   
    return 0; 
} 
```

# Longest Palindromic Substring

The time complexity can be reduced by storing results of subproblems.
We maintain a boolean table[n][n] that is filled in bottom up manner. The value of table[i][j] is true, if the substring is palindrome, otherwise false. To calculate table[i][j], we first check the value of table[i+1][j-1], if the value is true and str[i] is same as str[j], then we make table[i][j] true. Otherwise, the value of table[i][j] is made false.

Time complexity: O ( n^2 )

Auxiliary Space: O ( n^2 )

```
# Python program 
  
import sys 
  
# A utility function to print a 
# substring str[low..high] 
def printSubStr(st,low,high) : 
    sys.stdout.write(st[low : high + 1]) 
    sys.stdout.flush() 
    return '' 
  
# This function prints the longest palindrome 
# substring of st[]. It also returns the length 
# of the longest palindrome 
def longestPalSubstr(st) : 
    n = len(st) # get length of input string 
  
    # table[i][j] will be false if substring  
    # str[i..j] is not palindrome. Else  
    # table[i][j] will be true 
    table = [[0 for x in range(n)] for y 
                          in range(n)]  
      
    # All substrings of length 1 are 
    # palindromes 
    maxLength = 1
    i = 0
    while (i < n) : 
        table[i][i] = True
        i = i + 1
      
    # check for sub-string of length 2. 
    start = 0
    i = 0
    while i < n - 1 : 
        if (st[i] == st[i + 1]) : 
            table[i][i + 1] = True
            start = i 
            maxLength = 2
        i = i + 1
      
    # Check for lengths greater than 2.  
    # k is length of substring 
    k = 3
    while k <= n : 
        # Fix the starting index 
        i = 0
        while i < (n - k + 1) : 
              
            # Get the ending index of  
            # substring from starting  
            # index i and length k 
            j = i + k - 1
      
            # checking for sub-string from 
            # ith index to jth index iff  
            # st[i+1] to st[(j-1)] is a  
            # palindrome 
            if (table[i + 1][j - 1] and 
                      st[i] == st[j]) : 
                table[i][j] = True
      
                if (k > maxLength) : 
                    start = i 
                    maxLength = k 
            i = i + 1
        k = k + 1
    print "Longest palindrome substring is: ",printSubStr(st, start, 
                                               start + maxLength - 1) 
  
    return maxLength # return length of LPS 
  
  
# Driver program to test above functions 
st = "forgeeksskeegfor"
l = longestPalSubstr(st) 
print "Length is:", l 
```