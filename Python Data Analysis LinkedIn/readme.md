# Python: Data Analysis

https://www.linkedin.com/learning/python-data-analysis/

## Packages Used

* numpy

* matplotlib
* pandas
* IPython
* seaborn

**Printing list**

```
for index, value in enumerate(list_name):
    print("Element",index,"->",value)
```

**Dictionary Recap**

```
capitals = {'United States': 'Washington, DC','France': 'Paris','Italy': 'Rome'}

'Germany' in capitals #False

morecapitals = {'Germany': 'Berlin','United Kingdom': 'London'}

capitals.update(morecapitals)
capitals

del capitals['United States']
capitals

for key in capitals:
    print(key,capitals[key])

for key in capitals.keys():
    print(key)

for value in capitals.values():
    print(value)

for key,value in capitals.items():
    print(key,value)
```

**Transposing a dictionary**

```
capitals = {'United States': 'Washington, DC','France': 'Paris','Italy': 'Rome'}
capitals_bycapital = {capitals[key]: key for key in capitals}

# {'Paris': 'France', 'Rome': 'Italy', 'Washington, DC': 'United States'}
```

