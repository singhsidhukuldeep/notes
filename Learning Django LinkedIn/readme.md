# Learning Django

https://www.linkedin.com/learning/learning-django

Django is a web framework for python

## Django Provides

The tools that Django comes with are an **object-relational mapper, called an ORM**, which helps us make database queries. Django also comes with **URL routing** which helps determine what logic to follow depending on the URL of a web request.
Another feature is **HTML templating** which allows us to have presentation logic into insert data into our HTML. Some other tools that come with Django are **form handling** and **unit testing tools** as well as some other smaller features. 

## Django is NOT

* A programing language

* A web server

## Install Django

```
pip3 install django
```

## Setting up

Go to the folder where you want to create your project

```
django-admin.py startproject projectName
```

## Files

* `manage.py`*  Runs Commands 

* `shelter/__init__.py`*  Tells Python that the folder contains python files

* `shelter/wsgi.py`*  Provides a hook for web servers like apache or engineX

* `shelter/settings.py`  Configures Django

* `shelter/urls.py`  Routes requests based on Urls

(*) Mostly unedited

## Running 

```
cd projectName
python3 manage.py runserver
```

check `localhost:8000`

```
python3 startapp AppName
```

Go to `shelter/settings.py` and in INSTALLED_APPS add your AppName

![Pieces of apps](images/piecesOfApps.png)


**Django uses an MVC, or model view controller architecture, however, Django uses some different names for these. The four pieces to understand are URL patterns, view, models, and templates.**

![Information Flow](images/infoFlow.png)

## Models

Models create the data layer of a given Django app. This means that they define the structure of our data and how it will be stored in the database. We will also use our models to leverage Django's ORM when creating data from the database. Typically a Django app contains a `models.py` file such as our adoptions app, which can contain any number of models. A model is a class that inherits from Django.db.models.Model. Models use class attributes to define their fields. As a rough analogy, we can conceptualize models as spreadsheets.

